gas = {definitions={}}

gas.default_def = {
    description = "gas",
    tiles = {"gas.png^[colorize:#E0E0E0CC"};
    on_construct = function(pos) 
        gas.set_concentration(pos, gas.get_name(pos), 256) 
    end,
    groups = {gas=1},
    paramtype = "light",
    sunlight_propagates = "true",
    drawtype = "glasslike",
    buildable_to = true,
    pointable = false,
    diggable = false,
    walkable = false,
    post_effect_color = {a = 180, r = 120, g = 120, b = 120},
    alpha = 20,
}

function gas.get_concentration(pos)
    local name = minetest.get_node(pos).name
    if name:sub(1,4) ~= "gas:" then
        return -1
    end
    return minetest.get_meta(pos):get_int("gas:concentration")
end

minetest.register_chatcommand("pressure", {
	description = "prints the gas name and pressure",
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if player then
		    local pos = player:get_pos()
		    pos = {x=pos.x, y=pos.y+1, z=pos.z}
		    local concentration = gas.get_concentration(pos)
		    if concentration == -1 then
		        minetest.chat_send_player(name, "you are not inside any gas")
		    else
		        minetest.chat_send_player(name, "gas ".. gas.get_name(pos)..", pressure is "..concentration)
		    end
		end
	end
})

function gas.set_concentration(pos, gas_name, value)
    local meta = minetest.get_meta(pos)
    meta:set_int("gas:concentration", value)
    meta:set_string("gas:name", gas_name)
    meta:set_int("gas:mod_time", math.floor(os.time()))
end

function gas.add_concentration(pos, gas_name, value)
    local current_value = 0
    if gas.get_name(pos) ~= gas_name then
	if minetest.get_node(pos).name ~= "air" then
            return -1
	end
        minetest.set_node(pos, {name=gas_name})
    else
        current_value = minetest.get_meta(pos):get_int("gas:concentration")
    end
    local meta = minetest.get_meta(pos)
    meta:set_int("gas:concentration", current_value+value)
    meta:set_string("gas:name", gas_name)
    meta:set_int("gas:mod_time", math.floor(os.time()))
    return 0
end
 
function gas.check_modified(pos)
        local meta = minetest.get_meta(pos)
        if (math.floor(os.time()) - meta:get_int("gas:mod_time")) == 0 then 
            return true
        end
        return false
end

function gas.get_name(pos)
    return minetest.get_node(pos).name
end

function gas.balance(pos)
        -- first we check if that gas part was updated shortly, if yes we do nothing
        if gas.check_modified(pos) then
            return
        end
        
        
        -- then we get the gas name, and check for all gas of that name in the 3x3x3 Block around it
        local meta = minetest.get_meta(pos)
        
        local metatable = meta:to_table()
        meta:from_table(metatable)

        local gas_name = meta:get_string("gas:name")
        local minp = {x=pos.x-1, y=pos.y-1, z=pos.z-1} 
        local maxp = {x=pos.x+1, y=pos.y+1, z=pos.z+1}
        local nodes = minetest.find_nodes_in_area(minp, maxp, {gas_name, "air"})

        -- next we calculate the average concentration of all the gas in the block
        local count = 0;
        local concentration_sum = 0;
        for _,gas_pos in pairs(nodes) do
            local concentration = minetest.get_meta(gas_pos):get_int("gas:concentration")
            concentration_sum = concentration_sum + concentration
            count = count + 1
        end
        
        local concentration = math.floor(concentration_sum/count)
        local remainder = concentration_sum % count -- we need the remainder to make sure no gas goes lost
        local remaining = 1 -- this var checks if there is still something of the remainder not put into one block
        
        -- we put in every block in the area the average concentration + 1 if 
        -- there is something of the remainder remaining
        while nodes do
            -- we choose a randmo block to prevent 
            -- that the behaviour is everytime the same
            if #nodes < 1 then break end
            local rnd = math.random(#nodes)
            local gas_pos = nodes[rnd]

            -- we delete the chosen block from the table, so that the loop 
            -- becomes finite
            table.remove(nodes, rnd)

            -- we check whether ther is something of the remainder remaining
            if (remainder <= 0) then remaining = 0 end
            remainder = remainder - 1

            -- we set the new value for the block
            -- if it is to low the gas may perish by a chance of 1/2
            local new_val = concentration+remaining
            if (new_val > 1) or ((new_val == 1) and (math.random(2) == 1)) then
                minetest.set_node(gas_pos, {name=gas_name})
                gas.set_concentration(gas_pos, gas_name, concentration+remaining)
            else
                -- this is just to prevent bugs
                minetest.set_node(gas_pos, {name="air"})
            end
        end

end

minetest.register_abm(
{
    label = "gas_expand",
    nodenames = {"group:gas"},
    neighbors = {"air"},
    interval = 1,
    chance = 3,
    action = function(pos, node)
        gas.balance(pos)
    end,
})

minetest.register_abm(
{
    label = "gas_inner_balance",
    nodenames = {"group:gas"},
    interval = 5,
    chance = 27,
    action = function(pos, node)
        gas.balance(pos)
    end,
})


if enable_fire then

    minetest.register_abm(
    {
        label = "gas_burn",
        nodenames = {"group:gas"},
        neighbors = {"fire:basic_flame"},
        interval = 1,
        chance = 1,
        action = function(pos, node)
            if minetest.get_item_group(node.name, "flammable") < 1 then return end
            local fire_pos = minetest.find_node_near(pos, 1, {"air"})
            if fire_pos == nil then return end
            minetest.set_node(fire_pos, {name="fire:basic_flame"})
        end,
    })
end

function gas.register_gas(name, def, perish_chance)

    local gas_def = table.copy(gas.default_def)
    for key,value in pairs(def) do
        gas_def[key] = value
    end
    
    gas_def.groups["gas"]=1 -- must have

    minetest.register_node(name,gas_def)
    
    gas.definitions[name] = {}
    gas.definitions[name].perish_chance = perish_chance
end

gas.register_gas("gas:oxygen", {tiles = {"gas.png^[colorize:#B0B0FFCC"}, post_effect_color = { a=70, r=180,g=180,b=250}}, 2)
gas.register_gas("gas:poison", {tiles = {"gas.png^[colorize:#33BB33CC"}, post_effect_color = {a = 180, r = 20, g = 120, b = 20}, damage_per_second = 5}, 2)

minetest.register_craft({
    type = "fuel",
    recipe = "gas:poison",
    burntime = 8,
})

